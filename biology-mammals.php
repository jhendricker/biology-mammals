<?php
/*
Plugin Name: Biology Mammals
Plugin URI: http://sciences.ucf.edu/it
Description: A plugin to add the Mammals Functionality to any theme. Requries ACF plugin and a page with the URL mammal-results. Search shortcode is: [show_cos_mammals] 
Version: 1.0.0
Author: COS Web Team
Author URI: http://sciences.ucf.edu
License: GPL2
Text Domain: biology-mammals
*/

/****************************
 * Custom Scirpts
 ****************************/
function cos_mammal_load_custom_script() {

    wp_register_script('cos_mammals_ajax', plugins_url('/js/cos_mammals_ajax.js', __FILE__ ), array('jquery') );
    // Finding the WP admin-ajax.php location and setting it to myAjax.ajaxurl
    // Setting a nonce to be passed and checked for
    wp_localize_script( 'cos_mammals_ajax', 'myAjax', array( 
    	'ajaxurl' => admin_url( 'admin-ajax.php' ), 
    	'ajax_nonce' => wp_create_nonce('College of Sciences Biology Mammals Nonce'),
    	)
    ); 

    wp_register_style( 'cos_bio_mammal_styling', plugins_url( '/css/biology-mammals.css', __FILE__ ) );
    wp_enqueue_style( 'cos_bio_mammal_styling' );

    wp_enqueue_script('jquery');
	wp_enqueue_script('cos_mammals_ajax');
}
add_action('wp_print_scripts', 'cos_mammal_load_custom_script');


/***************
 * Ajax action
 **************/
// If logged
add_action('wp_ajax_cos_mammal_code', 'cos_mammal_populate_select');
// If not logged in use this:
add_action('wp_ajax_nopriv_cos_mammal_code', 'cos_mammal_populate_select');

/**************************************
 * Code for  Ajax COS Mammal Functions
 **************************************/
function cos_mammal_populate_select(){  

	check_ajax_referer( 'College of Sciences Biology Mammals Nonce', 'security' );
  	// Function call if the Infraclass select item is changed
  	echo cos_mammal_tax_filter($_POST['parentTax'], $_POST['parentTaxTitle'], $_POST['childTax']);
  exit();
}
/**************************************************************
 * Ajax function for the Mammal Search functionality that creates a dropdown list of valid "child" taxonomy items based on the "parent" taxonomy's selection.  The taxonomies aren't actually hierarchical.
 **************************************************************/
function cos_mammal_tax_filter($parentTaxSelected = "", $parentTax = 'cos_mammal_infraclass', $childTax = 'cos_mammal_order'){

  // Gets a list of all the taxonomies  
  $mammal_tax_args = array(
   'orderby' => 'name',
   'order'   => 'ASC',
   'hide_empty'=> true    
  );
  $mammal_taxonomies = get_terms($childTax, $mammal_tax_args);

  $valid_items = array();

  if( ! empty( $mammal_taxonomies ) && ! is_wp_error( $mammal_taxonomies ) ){   
  foreach ($mammal_taxonomies as $mammal_taxonomy) {
    if(!empty($parentTaxSelected) ){
      /* Loop through each taxonomy item and build a query to see if it has any items in common w/ the "parent" taxonomy */
      $combo_args = array(
       'post_type'       =>  'cos_mammals',
       'posts_per_page'  =>  -1,
       'tax_query'       =>  array(
          'relation'    =>  'AND',
          array(
            'taxonomy'  =>  $parentTax,
            'field'     =>  'slug',
            'terms'     =>  $parentTaxSelected,
          ),
          array(
            'taxonomy'  =>  $childTax,
            'field'     =>  'slug',
            'terms'     =>  $mammal_taxonomy->slug,
          ),
       ),
      );
    } else {
      /* If the select item triggering the change doesn't have a value, i.e. "-Any ____-" then disable the "child" dropdown box */
      $selectName = str_replace("cos_mammal_", "", $childTax);

      ob_start();

      echo "<select id='$childTax' name='mammal$selectName' class='mammal_types_drop form-control disabled' >";
      echo  "<option value=''>-Any ".ucfirst($selectName)."-</option></select>";          

      $Content = ob_get_clean();
      return $Content;
      exit();
    }
    $order_query = new WP_Query( $combo_args );

    /* If common items are found then add that "child" taxonomy to an array of valid "child" taxonomies */
    if($order_query->have_posts())
      $valid_items[] = $mammal_taxonomy->name;
   }

   ob_start();
   
   /* If there are valid "child" taxonomies then create a Select dropdown list with them */
   if(!empty($valid_items)){

    $selectName = str_replace("cos_mammal_", "", $childTax);
    
    echo "<select id='$childTax' name='mammal$selectName' class='mammal_types_drop form-control' >";
    echo  "<option value=''>-Any ".ucfirst($selectName)."-</option>";
    foreach($valid_items as $valid_item){     
      echo "<option value='".strtolower($valid_item)."'>".$valid_item."</option>";
    }
    echo "</select>";
   }

   $Content = ob_get_clean();
  } else {
   $Content = "There are no items found at this time";
  }
  return $Content;
}


// *****************************************************
// * Custom Titles for CPTs that don't use Title field
// *****************************************************
function cos_mammal_custom_titles($title) {
	$postID   = get_the_ID();
	$postType = get_post_type( $postID );
	
	/* Note that the second field in the $_POST['acf'][***] item will vary from installation to installation */
	if ($postType == 'cos_mammals') {
   		$title = $_POST['acf']['field_547f18a090b4b'];
  	}

	return $title;
}
add_filter('title_save_pre','cos_mammal_custom_titles');


// ********************************
// * Custom Post Type for Mammals
// ********************************
function cos_mammals() {
  
  $labels = array(
    'name' => _x('Mammals', 'post type general name'),
    'singular_name' => _x('Mammal', 'post type singular name'),
    'add_new' => _x('Add New', 'mammal'),
    'add_new_item' => __('Add New Mammal'),
    'edit_item' => __('Edit Mammal Info'),
    'new_item' => __('New Mammal'),
    'all_items' => __('All Mammals'),
    'view_item' => __('View Mammal'),
    'search_items' => __('Search All Mammals'),
    'not_found'  => __('No Mammals found.'),
    'not_found_in_trash'  => __('No Mammals found in Trash.'),
    'parent_item_colon' => '',
    'menu_name'  => __('Mammals'),
  );

  $args = array(
    'labels' => $labels,
    'singular_label' => __('Mammal'),
    'public' => true,
    'show_ui' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'rewrite' => array( 'slug' => 'mammal' ),
    'supports' => array('custom-fields'),
    'taxonomies' => array('cos_mammal_infraclass, cos_mammal_order, cos_mammal_family, cos_mammal_genus'),
  );

  register_post_type( 'cos_mammals', $args );
}
add_action('init', 'cos_mammals');


// **********************************************
// * Custom taxonomies for Mammal classifications
// **********************************************

// *** Mammal Order *** //
function cos_mammal_infraclass() {
  // create a new taxonomy
  register_taxonomy(
    'cos_mammal_infraclass',
    'cos_mammals',
    array(
      'labels' => array(
        'name' => __('Infraclass'),
        'add_new_item'  => __('Add New Infraclass'),
        'parent_item'   => __('New Infraclass Parent'),
        'search_items'  =>  __('Search Infraclass'),
      ),
      'sort' => true,
      'hierarchical' => true,
      'args' => array( 'orderby' => 'term_order' ),
      'query_var' => true,
      'rewrite' => false, /*array( 'slug' => 'group' )*/

    )
  );
}
add_action( 'init', 'cos_mammal_infraclass' ); 

// *** Mammal Order *** //
function cos_mammal_order() {
  $labels = array(

  );
  // create a new taxonomy
  register_taxonomy(
    'cos_mammal_order',
    'cos_mammals',
    array(
      'labels' => array(
        'name' => __('Order'),
        'add_new_item'  => __('Add New Order'),
        'parent_item'   => __('New Order Parent'),
        'search_items'  =>  __('Search Order'),
      ),
      'sort' => true,
      'hierarchical' => true,
      'args' => array( 'orderby' => 'term_order' ),
      'query_var' => true,
      'rewrite' => array( 'slug' => 'order' ),
    )
  );
}
add_action( 'init', 'cos_mammal_order' ); 

// *** Mammal Family *** //
function cos_mammal_family() {
  // create a new taxonomy
  register_taxonomy(
    'cos_mammal_family',
    'cos_mammals',
    array(
      'labels' => array(
        'name'          => __('Family'),
        'add_new_item'  => __('Add New Family'),
        'parent_item'   => __('New Family Parent'),
        'search_items'  => __('Search Family'),
      ),
      'sort' => true,
      'hierarchical' => true,
      'args' => array( 'orderby' => 'term_order' ),
      'query_var' => true,
      'rewrite' => false, /*array( 'slug' => 'group' )*/
    )
  );
}
add_action( 'init', 'cos_mammal_family' ); 

// *** Mammal Genus *** //
function cos_mammal_genus() {
  // create a new taxonomy
  register_taxonomy(
    'cos_mammal_genus',
    'cos_mammals',
    array(
      'labels' => array(
        'name'          => __('Genus'),
        'add_new_item'  => __('Add New Genus'),
        'parent_item'   => __('New Genus Parent'),
        'search_items'  => __('Search Genus'),
      ),
      'sort' => true,
      'hierarchical' => true,
      'args' => array( 'orderby' => 'term_order' ),
      'query_var' => true,
      'rewrite' => false, /*array( 'slug' => 'group' )*/
    )
  );
}
add_action( 'init', 'cos_mammal_genus' ); 

/* Remove/hide the mammal taxonomy metaboxes from appearing
 * on the Mammals CPT page. This forces them to use the
 * ACF fields that will save to the taxonomy on creation/update
 */
function remove_mammal_taxonomies(){
  // Since we are using Hierarchical taxomies we must use {taxonomy_name}div
  remove_meta_box('cos_mammal_infraclassdiv', 'cos_mammals', 'side' );
  remove_meta_box('cos_mammal_orderdiv', 'cos_mammals', 'side' );
  remove_meta_box('cos_mammal_familydiv', 'cos_mammals', 'side' );
  remove_meta_box('cos_mammal_genusdiv', 'cos_mammals', 'side' );
}
add_action( 'admin_menu', 'remove_mammal_taxonomies' );

// ******** End Mammals Taxonomy Info ********

/* Displays the form for selecting the mammal Infraclass, 
 * Order, Family, and Genus
 */
function cos_show_mammals(){

  $mammal_infraclass_args = array(
   'orderby' => 'name',
   'order'   => 'ASC',
   'hide_empty'=> true    
  );
  $mammal_infraclasses = get_terms('cos_mammal_infraclass', $mammal_infraclass_args);

  $mammal_order_args = array(
   'orderby' => 'name',
   'order'   => 'ASC',
   'hide_empty'=> true    
  );
  $mammal_orders = get_terms('cos_mammal_order', $mammal_order_args);

  $mammal_family_args = array(
   'orderby' => 'name',
   'order'   => 'ASC',
   'hide_empty'=> true    
  );
  $mammal_families = get_terms('cos_mammal_family', $mammal_family_args);

  $mammal_genus_args = array(
   'orderby' => 'name',
   'order'   => 'ASC',
   'hide_empty'=> true    
  );
  $mammal_genera = get_terms('cos_mammal_genus', $mammal_genus_args);  

  ob_start();
?>
  <form action="<?php bloginfo('url'); ?>/results/" method="post" id="mammal_search">
<?php

  if(!empty($mammal_infraclasses)): 
   echo "<h5>Select Infraclass:</h5>";
   echo "<div id='infraclass_dropdown'><select id='cos_mammal_infraclass' name='mammalinfraclass' class='mammal_types_drop form-control'>";
   echo  "<option value=''>-Any Infraclass-</option>";
   foreach($mammal_infraclasses as $mammal_infraclass){     
    echo "<option value='".$mammal_infraclass->slug."'>".$mammal_infraclass->name."</option>";
   }
   echo "</select></div>";
  endif;

  if(!empty($mammal_orders)): 
   echo "<h5>Select Order:</h5>";
   echo "<div id='order_dropdown'><select id='cos_mammal_order' name='mammalorder' class='mammal_types_drop form-control' disabled>";
   echo  "<option value=''>-Any Order-</option>";
   /*foreach($mammal_orders as $mammal_order){     
    echo "<option value='".$mammal_order->slug."'>".$mammal_order->name."</option>";
   }*/
   echo "</select></div>";
  endif;

  if(!empty($mammal_families)): 
   echo "<h5>Select Family:</h5>";    
   echo "<div id='family_dropdown'><select id='cos_mammal_family' name='mammalfamily' class='mammal_types_drop form-control'  disabled>";
   echo  "<option value=''>-Any Family-</option>";
   /*foreach($mammal_families as $mammal_family){     
    echo "<option value='".$mammal_family->slug."'>".$mammal_family->name."</option>";    
   }*/
   echo "</select></div>";
  endif;  

  if(!empty($mammal_genera)):  
   echo "<h5>Select Genus:</h5>";    
   echo "<div id='genus_dropdown'><select id='cos_mammal_genus' name='mammalgenus' class='mammal_types_drop form-control' disabled>";
   echo  "<option value=''>-Any Genus-</option>";
   /*foreach($mammal_genera as $mammal_genus){           
    echo "<option value='".$mammal_genus->slug."'>".ucfirst($mammal_genus->slug)."</option>";     
   } */  
   echo "</select></div>";
  endif;    

   /*foreach($group_types as $group_type){
    echo "<label for='type-".$group_type->slug."'>".$group_type->name."</label>";
    echo "<input type='checkbox' name='type-".$group_type->slug."' value='".$term->name."' />";
   }*/
  if(!empty($mammal_orders) || !empty($mammal_families) || !empty($mammal_genera)){
   echo ' <input type="submit" id="searchsubmit" value="Search Mammals" />
   </form>';
  }else{
   echo "<p><strong>There are no Mammals at this time, please check back later</strong></p>";
  }

  echo "<hr/>";

  $groupContent = ob_get_clean();
  return $groupContent;
}
add_shortcode('show_cos_mammals', 'cos_show_mammals');


/* Recursive funciton to loop through Mammal photos 
 * Takes in the server path and website path to files
 */
function cos_mammal_pictures($base_dir, $normal_dir, $photos_to_show = ""){  

  $folder = $base_dir;
  $normal_upload_dir = $normal_dir;

  $photos_to_show .= "";    

  if(is_dir($folder)){

   // Scan the folder looking for photos or sub folders
   $directory_photos = scandir($folder);    

   // If there are photos or sub folders
   if($directory_photos !== false){
    
    $photos_to_show .= "<div class='mammal_photo_container'>";

    // Go through each entry looking for a photo or another folder
    foreach ($directory_photos as $photo => $value) {     

      // Remove the Previous and Up a Level entries 
      if (!in_array($value,array(".",".."))){ 
       
       // If the entry is another folder recursively 
       // call yourself with updated variables
       if(is_dir($folder.$value)){                
        $new_base   = $folder . $value."/";
        $new_normal = $normal_upload_dir . $value."/"; 
        $photos_to_show .=  "<h4>".ucwords($value)."</h4>".cos_mammal_pictures($new_base, $new_normal);
       }
       // If it's the full size image
       elseif(strpos($value, '_sm') === false){
        if(strpos($value, '.jpg') !== false || strpos($value, '.JPG') !== false)
          $full_size_photo = $value;
       }       
       // If it's the thumbnail
       elseif(strpos($value, '_sm') !== false){
        $photos_to_show .=  "<a href='$normal_upload_dir$full_size_photo'><img src='$normal_upload_dir$value' class='specimen_photo'/></a> ";
       }                   
      }

    } // Foreach      

    $photos_to_show .= "</div>";
   }
  }
  
  return $photos_to_show; 
}
// ******** End Mammals CPT Info ********

// Function to use custom single-cos-mammals.php file instead of the theme default template file
function cos_mammal_single_cpt_template($single_template) {
     global $post;

     if ($post->post_type == 'cos_mammals') {
          $single_template = dirname( __FILE__ ) . '/single-cos_mammals.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'cos_mammal_single_cpt_template' );

function cos_mammal_taxonomy_cpt_template($tax_template) {
     global $post;

     if ($post->post_type == 'cos_mammals') {
          $tax_template = dirname( __FILE__ ) . '/taxonomy-cos_mammal_order.php';
     }
     return $single_template;
}
add_filter( 'taxonomy_template', 'cos_mammal_taxonomy_cpt_template' );

// ****************************************
// * Custom Pagination from Kriesi.at
// * http://www.kriesi.at/archives/how-to-build-a-wordpress-post-pagination-without-plugin
// ****************************************
function cos_mammal_kriesi_pagination($pages = '', $range = 2){  

     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == ''){
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages){
             $pages = 1;
         }
     }   

     if(1 != $pages){

         echo "<div class='kriesi-pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++){
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

 
/******************************************************/
/******************************************************/
/******************************************************/
/******************************************************/
/******************************************************/


class PageTemplater {

	/**
	 * A reference to an instance of this class.
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 */
	protected $templates;

	/**
	 * Returns an instance of this class. 
	 */
	public static function get_instance() {

		if ( null == self::$instance ) {
			self::$instance = new PageTemplater();
		} 

		return self::$instance;

	} 

	/**
	 * Initializes the plugin by setting filters and administration functions.
	 */
	private function __construct() {

		$this->templates = array();


		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}

		// Add a filter to the save post to inject out template into the page cache
		add_filter(
			'wp_insert_post_data', 
			array( $this, 'register_project_templates' ) 
		);


		// Add a filter to the template include to determine if the page has our template assigned and return it's path
		add_filter(
			'template_include', 
			array( $this, 'view_project_template') 
		);


		// Add your templates to this array.
		$this->templates = array(
			'mammal_results-page.php' => 'Mammal Results',
		);
			
	} 

	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
	 */
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. 
		// If it doesn't exist, or it's empty prepare an array
		$templates = wp_get_theme()->get_page_templates();
		if ( empty( $templates ) ) {
			$templates = array();
		} 

		// New cache, therefore remove the old one
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	} 

	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		
		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta( 
			$post->ID, '_wp_page_template', true 
		)] ) ) {
			return $template;
		} 

		$file = plugin_dir_path( __FILE__ ). get_post_meta( 
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

} 
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );